/*
 * rosserial PubSub Example
 * Prints "hello world!" and toggles led
 */

#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>
#include <geometry_msgs/PoseStamped.h>
//boolean latency_bool_msg = false;


ros::NodeHandle  nh;


geometry_msgs::PoseStamped latency_bool_msg;
ros::Publisher chatter("latency_bool_fdbk", &latency_bool_msg);

void messageCb( const geometry_msgs::PoseStamped& latency_bool_msg){
  chatter.publish( &latency_bool_msg );
  nh.spinOnce();
}

ros::Subscriber<geometry_msgs::PoseStamped> sub("latency_bool", messageCb );



void setup()
{
  //pinMode(13, OUTPUT);
  nh.initNode();
  nh.advertise(chatter);
  nh.subscribe(sub);
}

void loop()
{
  nh.spinOnce();
  //int a = 1;
}
