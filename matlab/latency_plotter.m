clear all;
clc
close all

load latencytest.txt

startrow = 1000

offset = (latencytest(startrow:end,1)-latencytest(startrow:end,2))*10^-9;

time =( latencytest(startrow:end,1)-latencytest(startrow,1))*10^-9;

plot(time,offset,'k')
xlabel('time (s)')
ylabel('ofset (s)')

saveas(gcf,'latency_timestamp.jpg')
