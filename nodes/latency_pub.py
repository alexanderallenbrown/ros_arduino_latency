#!/usr/bin/env python
import rospy
from geometry_msgs.msg import PoseStamped


def talker():
    pub = rospy.Publisher('latency_bool', PoseStamped)
    rospy.init_node('talker')
    lat_msg = PoseStamped()
    while not rospy.is_shutdown():
        lat_msg.header.stamp= rospy.Time.now()
        #rospy.loginfo(lat_msg)
        pub.publish(lat_msg)
        rospy.sleep(.1)


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
